# docker-nginx-php-symfony-react

tbd.

## Getting started

tbd.

### Requirements
***

* Docker Desktop
* Docker compose
* IDE (preferably PhpStorm)

### Stack
***

The following stack is used in this project:

* nginx 1.24
* PHP 8.2
* Symfony 6.3
* Node 18.16
* React 18.2

### Project Structure
***

The following project structure is provided:

| dir/file name         | purpose |
|-----------------------|---------|
| `/api`                | tbd.    | 
| `/docker`             | tbd.    |
| `/var`                | tbd.    |
| `/web-ui`             | tbd.    |
| `.gitlab-ci.yml`      | tbd.    |
| `docker-compose.yml ` | tbd.    |

### Installation
*** 

Run these commands to start the project:

1. If not already done, install [Docker Desktop](https://www.docker.com/products/docker-desktop/) and [Docker Compose](https://docs.docker.com/compose/install/)
2. Run `docker compose build --pull --no-cache` to build fresh images
3. Run `docker compose up -d` or `docker compose up` (so the logs will be displayed in the current shell)
4. Open http://localhost:90/ to see the symfony backend and http://localhost:3000/ to view the React frontend

## Usage

### Project setup
***

All dependencies for the backend and frontend part are directly **INSTALLED INSIDE THE CONTAINER**. But the setup is design so that you will be able to see these folders/files in our host machine, too.

If you want to install new libraries or dependencies you have to install them directly in the container.

This way we can assure that the docker environment is reproducible on any machine.

### CORS
***

To avoid [Cross-Origin Resource Sharing](https://enable-cors.org/) issues in the local environment the nginx api config was adjusted to enable preflight requests see https://enable-cors.org/server_nginx.html.

### Commands
***

| name  | purpose |
|-------|---------|
| `xxx` | tbd.    | 
| `xxx` | tbd.    |
| `xxx` | tbd.    |
| `xxx` | tbd.    |
| `xxx` | tbd.    | 
| `xxx` | tbd.    |


## Using Xdebug

[Xdebug](https://xdebug.org/) is a popular debugger and profiler for PHP. It's mainly used for going through a program step by step and it allows to pause the execution of the applications to inspect the state of everything

The following snippet contains all configuration for Xdebug:
```yaml
RUN docker-php-ext-enable xdebug \
    && echo "xdebug.client_port=9000" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.discover_client_host=false" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.mode=develop,debug" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.log=/var/log/xdebug.log" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.log_level=0" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
    && echo "xdebug.start_with_request=yes" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
```

### Debugging with Xdebug and PHPStorm
***

First, [create a PHP debug remote server configuration](https://www.jetbrains.com/help/phpstorm/creating-a-php-debug-server-configuration.html):

1. In the `Settings/Preferences` dialog, go to `PHP | Servers`
2. Create a new server:
   * Name: `localhost` (or whatever you want to use)
   * Host: `localhost` (or the one defined in nginx conf)
   * Port: `90`
   * Debugger: `Xdebug`
   * Check `Use path mappings`
   * Absolute path on the server: `/var/www/api`

Now go to `Run | Edit Configuration` and add a new `PHP Remote Debug`:
* Name: `Xdebug` (or whatever you want to use)
* Server: `localhost` (the one defined before)
* IDE key(session id): `PHPSTORM` (defined in the Dockerfile -> `xdebug.idekey=PHPSTORM`)

You can now set breakpoints and use the debugger!

### Troubleshooting
***

Inspect the installation with the following command. The Xdebug version should be displayed.

```console
$ docker exec docker-nginx-php-symfony-react-api-1 php -v  

PHP ...
    with Xdebug v3.x.x ...
```

If your IDE didn’t stop at the breakpoint that you set, then you’ll have to [troubleshoot](https://xdebug.org/docs/step_debug#troubleshoot) what might have gone wrong, by consulting Xdebug’s diagnostic log.

```php
xdebug_info();
exit;
```

## Using API Platform

[API Platform](https://api-platform.com/) is a full stack framework dedicated to API-driven projects and architectures.

### Entities

API Platform is able to automatically expose entities mapped as "API resources" through a REST API supporting CRUD operations. To expose your entities, you can use Docblock annotations, XML and YAML configuration files.

An example can be found here [User.php](link-to-repo)



## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
